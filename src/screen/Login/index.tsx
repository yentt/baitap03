import React, {useEffect, useState, useMemo, useCallback} from 'react';
import {
  Alert,
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  TextInput,
} from 'react-native';
import {MyButton, styleButton} from '../../component/MyButton';

export function Login() {
  const [userName, setUserName] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errorUserName, setErrorUserName] = useState<string>('');
  const [errorPassword, setErrorPassword] = useState<string>('');

  useEffect(() => {
    Alert.alert('Chao mung ban da den voi Yolo System');
    return () => {};
  }, []);

  const isValidLogin = userName.length > 0 && password.length > 0;
  const LoginButtonStyle = useMemo(() => {
    if (isValidLogin) {
      return [styleButton.button, styleButton.buttonLogin];
    } else {
      return [styleButton.button, styleButton.buttonDisable];
    }
  }, [isValidLogin]);

  const LoginButtonDisable = useMemo(() => {
    if (isValidLogin) {
      return false;
    } else {
      return true;
    }
  }, [isValidLogin]);

  const loginPress = useCallback(() => {
    if (isValidLogin) {
      Alert.alert(
        'Xin chao ban ' + userName + ' da dang nhap thanh cong vao Yolo System',
      );
    }
  }, [isValidLogin, userName]);
  const onUserNameBlur = () => {
    if (userName.length === 0) {
      setErrorUserName('Ban phai nhap ten dang nhap');
      return errorUserName;
    } else {
      setErrorUserName('');
    }
  };
  const onPasswordBlur = () => {
    if (password.length === 0) {
      setErrorPassword('Ban phai nhap mat khau');
      return errorPassword;
    } else {
      setErrorPassword('');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.paddingContent}>
        <Text style={styles.titleStyle}>Yolo System</Text>
        <TextInput
          onChangeText={setUserName}
          style={styles.input}
          placeholder="Tên đăng nhập"
          onBlur={onUserNameBlur}
        />
        <Text style={styles.error}>{errorUserName}</Text>
        <TextInput
          onChangeText={setPassword}
          secureTextEntry={true}
          style={styles.input}
          placeholder="Mật khẩu"
          onBlur={onPasswordBlur}
        />
        <Text style={styles.error}>{errorPassword}</Text>
        <MyButton
          onPress={loginPress}
          style={LoginButtonStyle}
          disabled={LoginButtonDisable}
          buttonText="Login"
        />
        <Text style={styles.titleStyle}>Or</Text>
        <MyButton
          style={[styleButton.button, styleButton.buttonFacebook]}
          buttonText="Facebook"
        />
        <MyButton
          style={[styleButton.button, styleButton.buttonGoogle]}
          buttonText="Google"
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  paddingContent: {
    borderColor: 'black',
    borderWidth: 1,
    paddingBottom: 15,
    marginHorizontal: 5,
  },
  titleStyle: {
    padding: 5,
    color: 'green',
    fontWeight: 'bold',
    fontSize: 50,
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  input: {
    padding: 10,
    height: 40,
    marginHorizontal: 15,
    borderWidth: 1,
    borderRadius: 10,
    marginTop: 10,
  },
  error: {
    marginHorizontal: 15,
    color: 'red',
  },
});
